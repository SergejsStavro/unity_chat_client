﻿using System.Collections;
using System.Collections.Generic;
using com.bitvillain.chat;
using com.bitvillain.chat.client;
using UnityEngine;

public class ChatModel
{
	public int GlobalRoomID = 0; //TODO fetch from server
	public int CurrentRoomID = 0; //TODO fetch from server
	public Dictionary<int, string> roomIDToMessages = new Dictionary<int, string>()
	{ 
		
		{ 0, string.Empty }

	};

	public Dictionary<string, RoomClientModel> currentRoomClients = new Dictionary<string, RoomClientModel>();
}
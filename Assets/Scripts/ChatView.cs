﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.bitvillain.chat.client;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatView : MonoBehaviour
{
	[SerializeField] TMP_InputField addressInputField;
	[SerializeField] TMP_InputField portInputField;
	[SerializeField] Button connectButton;

	[SerializeField] GameObject roomPanel;
	[SerializeField] GameObject connectPanel;
	[SerializeField] GameObject connectingPanel;
	[SerializeField] TMP_InputField messageInputField;
	[SerializeField] TMP_Text messagesText;
	[SerializeField] Button sendMessageButton;
	[SerializeField] TMP_InputField nameInputField;
	[SerializeField] Button setNameButton;
	[SerializeField] RoomPanelView[] roomPanels;
	[SerializeField] Button newRoomButton;
	[SerializeField] ClientPanelView[] globalClientPanels;
	[SerializeField] ClientPanelView[] privateClientPanels;

	public event Action<string, string> ConnectionRequested = (address, port) => { };
	public event Action<string> SendMessageRequested = (_) => { };
	public event Action<string> SetNameRequested = (_) => { };
	public event Action CreateNewRoomRequested = () => { };

	public event Action<int> SelectRoomRequested
	{
		add
		{
			if (roomPanels != null)
				foreach (var item in roomPanels)
					item.SelectClicked += value;
		}
		remove
		{
			if (roomPanels != null)
				foreach (var item in roomPanels)
					item.SelectClicked -= value;
		}
	}
	public event Action<int> LeaveRoomRequested
	{
		add
		{
			if (roomPanels != null)
				foreach (var item in roomPanels)
					item.LeaveClicked += value;
		}
		remove
		{
			if (roomPanels != null)
				foreach (var item in roomPanels)
					item.LeaveClicked -= value;
		}
	}

	public event Action<string> AddClientRequested
	{
		add
		{
			if (globalClientPanels != null)
				foreach (var item in globalClientPanels)
					item.AddClientClicked += value;
		}
		remove
		{
			if (globalClientPanels != null)
				foreach (var item in globalClientPanels)
					item.AddClientClicked -= value;
		}
	}

	void Awake()
	{
		connectButton.onClick.AddListener(OnConnectClicked);
		sendMessageButton.onClick.AddListener(OnSendMessageClicked);
		setNameButton.onClick.AddListener(OnSetNameClicked);
		newRoomButton.onClick.AddListener(OnNewRoomClicked);

		ClearGlobalClients();
		ClearPrivateClients();
		SetAddButtonsVisibility(false);
		for (int i = 0; i < privateClientPanels.Length; i++)
			privateClientPanels[i].SetAddButtonVisibility(false);
	}

	private void OnConnectClicked()
	{
		ConnectionRequested(addressInputField.text, portInputField.text);
	}

	private void OnNewRoomClicked()
	{
		CreateNewRoomRequested();
	}

	private void OnSetNameClicked()
	{
		SetNameRequested(nameInputField.text);
	}

	private void OnSendMessageClicked()
	{
		SendMessageRequested(messageInputField.text);
	}

	public void ClearMessage()
	{
		messageInputField.text = string.Empty;
	}

	internal void HandleStateEntered(ChatMachineState obj)
	{
		connectingPanel.SetActive(obj == ChatMachineState.Disconnected);
		connectPanel.SetActive(obj == ChatMachineState.Unconnected);
		roomPanel.SetActive(obj == ChatMachineState.Connected);
	}

	internal void UpdateMessages(string messages)
	{
		messagesText.text = messages;
	}

	internal void UpdateRooms(Dictionary<int, string> roomIDToMessages, int globalRoomID)
	{
		var roomIDs = new List<int>(roomIDToMessages.Keys);

		for (int i = 0; i < roomPanels.Length; i++)
		{
			roomPanels[i].gameObject.SetActive(i < roomIDs.Count);
			if (i < roomIDs.Count)
			{
				roomPanels[i].SetID(roomIDs[i]);
				roomPanels[i].SetLeaveButtonVisibility(roomIDs[i] != globalRoomID);
			}
		}
	}

	internal void SetName(string name)
	{
		nameInputField.text = name;
	}

	int globalRoomID = 0;
	internal void UpdateGlobalClients(Dictionary<string, RoomClientModel> currentRoomClients)
	{
		var clientTokens = new List<string>(currentRoomClients.Keys);

		for (int i = 0; i < globalClientPanels.Length; i++)
		{
			globalClientPanels[i].gameObject.SetActive(i < clientTokens.Count);
			if (i < clientTokens.Count)
			{
				globalClientPanels[i].SetData(clientTokens[i], currentRoomClients[clientTokens[i]].Name);
			}
		}
	}

	internal void UpdatePrivateClients(Dictionary<string, RoomClientModel> currentRoomClients)
	{
		var clientTokens = new List<string>(currentRoomClients.Keys);

		for (int i = 0; i < privateClientPanels.Length; i++)
		{
			privateClientPanels[i].gameObject.SetActive(i < clientTokens.Count);
			if (i < clientTokens.Count)
			{
				privateClientPanels[i].SetData(clientTokens[i], currentRoomClients[clientTokens[i]].Name);
			}
		}
	}

	internal void ClearGlobalClients()
	{
		for (int i = 0; i < globalClientPanels.Length; i++)
			globalClientPanels[i].gameObject.SetActive(false);
	}

	internal void ClearPrivateClients()
	{
		for (int i = 0; i < privateClientPanels.Length; i++)
			privateClientPanels[i].gameObject.SetActive(false);
	}

	internal void SetName(string token, string name)
	{
		for (int i = 0; i < privateClientPanels.Length; i++)
			if(privateClientPanels[i].Token == token) 
				privateClientPanels[i].SetData(token, name);


		for (int i = 0; i < globalClientPanels.Length; i++)
			if(globalClientPanels[i].Token == token) 
				globalClientPanels[i].SetData(token, name);
	}

	internal void SetAddButtonsVisibility(bool areOn)
	{
		for (int i = 0; i < globalClientPanels.Length; i++)
			globalClientPanels[i].SetAddButtonVisibility(areOn);
	}
}
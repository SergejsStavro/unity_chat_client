﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomPanelView : MonoBehaviour
{
	public int ID;
	[SerializeField] TMP_Text roomText;
	[SerializeField] Button roomSelectButton;
	[SerializeField] Button roomLeaveButton;

	public event Action<int> SelectClicked = (_) => {};
	public event Action<int> LeaveClicked = (_) => {};

	void Awake() {
		roomSelectButton.onClick.AddListener(OnSelectClicked);
		roomLeaveButton.onClick.AddListener(OnLeaveClicked);
	}

	private void OnSelectClicked()
	{
		SelectClicked(ID);
	}

	private void OnLeaveClicked()
	{
		LeaveClicked(ID);
	}

	internal void SetID(int id)
	{
		ID = id;
		roomText.text = string.Format(
			"Room #{0}", id
		);
	}

	public void SetLeaveButtonVisibility(bool isOn)
	{
		roomLeaveButton.gameObject.SetActive(isOn);
	}
}

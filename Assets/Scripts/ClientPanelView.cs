﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ClientPanelView : MonoBehaviour
{
	public string Token;
	[SerializeField] TMP_Text nameText;
	[SerializeField] Button roomAddButton;

	public event Action<string> AddClientClicked = (_) => { };

	void Awake()
	{
		roomAddButton.onClick.AddListener(OnAddClicked);
	}

	private void OnAddClicked()
	{
		AddClientClicked(Token);
	}

	internal void SetData(string token, string name)
	{
		Token = token;
		nameText.text = string.Format(
			"U: {0}", name
		);
	}

	public void SetAddButtonVisibility(bool isOn)
	{
		roomAddButton.gameObject.SetActive(isOn);
	}
}
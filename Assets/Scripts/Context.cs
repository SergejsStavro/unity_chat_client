﻿using System;
using System.Collections.Generic;
using com.bitvillain.chat.client;
using UnityEngine;

public class Context : MonoBehaviour
{
	[SerializeField] ChatView chatView;

	ChatModel chatModel = new ChatModel();
	ChatClient chatClient = new ChatClient();
	ChatStateMachine ChatStateMachine = new ChatStateMachine();

	void Awake()
	{
		chatClient.ClientConnected += HandleChatClientConnected;
		chatClient.ClientDisconnected += HandleChatClientDisconnected;
		chatClient.ClientConnectionLost += HandleChatClientConnectionLost;
		chatClient.ClientNameReceived += HandleClientNameReceived;
		chatClient.ClientRoomMessageReceived += HandleClientRoomMessageReceived;
		chatClient.ClientRoomListReceived += HandleClientRoomListReceived;
		chatClient.RoomClientListReceived += HandleRoomClientListReceived;

		ChatStateMachine.StateEntered += chatView.HandleStateEntered;

		chatView.ConnectionRequested += Connect;
		chatView.SendMessageRequested += SendMessage;
		chatView.SetNameRequested += SetName;
		chatView.CreateNewRoomRequested += CreateNewRoom;
		chatView.LeaveRoomRequested += LeaveRoom;
		chatView.SelectRoomRequested += SelectRoom;
		chatView.AddClientRequested += AddClient;
	}

	void Start()
	{
		ChatStateMachine.Initialize();
	}

	private void Connect(string address, string port)
	{
		if(address != string.Empty)
			chatClient.Hostname = address;
		if(port != string.Empty)
			chatClient.Port = Int32.Parse(port);
		chatClient.Connect();
	}

	private void AddClient(string token)
	{
		chatClient.AddToRoom(chatModel.CurrentRoomID, token);
	}

	private void SelectRoom(int roomID)
	{
		chatModel.CurrentRoomID = roomID;
		chatClient.FetchClientList(chatModel.CurrentRoomID);
		chatView.UpdateMessages(chatModel.roomIDToMessages[chatModel.CurrentRoomID]);
		if (chatModel.CurrentRoomID == chatModel.GlobalRoomID)
		{
			chatView.ClearPrivateClients();
			chatView.SetAddButtonsVisibility(false);
		}
		else
		{
			chatView.SetAddButtonsVisibility(true);
		}
	}

	private void LeaveRoom(int roomID)
	{
		chatClient.RemoveFromRoom(roomID, chatClient.ClientToken);
		if (chatModel.CurrentRoomID == roomID)
		{
			chatModel.CurrentRoomID = 0;
			chatView.UpdateMessages(chatModel.roomIDToMessages[chatModel.CurrentRoomID]);
			chatView.ClearPrivateClients();
			chatView.SetAddButtonsVisibility(false);
		}
	}

	private void CreateNewRoom()
	{
		chatClient.CreateNewRoom();
	}

	private void HandleClientRoomListReceived(ClientRoomListModel clientRoomList)
	{
		List<int> clientRoomListRoomIDs = new List<int>();

		for (int i = 0; i < clientRoomList.Rooms.Count; i++)
		{
			if (!chatModel.roomIDToMessages.ContainsKey(clientRoomList.Rooms[i].RoomID))
			{
				chatModel.roomIDToMessages[clientRoomList.Rooms[i].RoomID] = string.Empty;
			}
			clientRoomListRoomIDs.Add(clientRoomList.Rooms[i].RoomID);
		}

		var chatModelRoomIDs = new List<int>(chatModel.roomIDToMessages.Keys);
		foreach (var chatModelRoomID in chatModelRoomIDs)
		{
			if (!clientRoomListRoomIDs.Contains(chatModelRoomID))
			{
				chatModel.roomIDToMessages.Remove(chatModelRoomID);
			}
		}

		chatView.UpdateRooms(chatModel.roomIDToMessages, chatModel.GlobalRoomID);
	}

	private void HandleRoomClientListReceived(RoomClientListModel roomClientList)
	{
		List<string> roomClientListTokens = new List<string>();

		for (int i = 0; i < roomClientList.Clients.Count; i++)
		{
			var clientToken = roomClientList.Clients[i].Token;
			if (!chatModel.currentRoomClients.ContainsKey(clientToken))
			{
				chatModel.currentRoomClients[clientToken] = roomClientList.Clients[i];
			}
			else
			{
				chatModel.currentRoomClients[clientToken].Name = roomClientList.Clients[i].Name;
			}
			roomClientListTokens.Add(clientToken);
		}

		var chatModelClientTokens = new List<string>(chatModel.currentRoomClients.Keys);
		foreach (var chatModelClientToken in chatModelClientTokens)
		{
			if (!roomClientListTokens.Contains(chatModelClientToken))
			{
				chatModel.currentRoomClients.Remove(chatModelClientToken);
			}
		}

		if (roomClientList.RoomID == chatModel.GlobalRoomID)
		{
			chatView.UpdateGlobalClients(chatModel.currentRoomClients);
		}
		else
		{
			chatView.UpdatePrivateClients(chatModel.currentRoomClients);
		}
	}

	private void HandleClientRoomMessageReceived(ClientRoomMessageModel clientRoomMessage)
	{
		if (chatModel.roomIDToMessages.ContainsKey(clientRoomMessage.RoomID))
		{
			chatModel.roomIDToMessages[clientRoomMessage.RoomID] =
				string.Format("{0}\n{1}({2}):\n-{3}",
					chatModel.roomIDToMessages[clientRoomMessage.RoomID],
					clientRoomMessage.Name,
					clientRoomMessage.Timestamp.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"),
					clientRoomMessage.Message
				);

			if (chatModel.CurrentRoomID == clientRoomMessage.RoomID)
				chatView.UpdateMessages(chatModel.roomIDToMessages[clientRoomMessage.RoomID]);
		}
	}

	private void HandleClientNameReceived(ClientNameModel data)
	{
		chatView.SetName(data.Token, data.Name);
	}

	private void SendMessage(string message)
	{
		chatClient.SendRoomMessage(chatModel.CurrentRoomID, message);
		chatView.ClearMessage();
	}

	private void SetName(string name)
	{
		chatClient.SetName(name);
	}

	private void HandleChatClientDisconnected()
	{
		if(ChatStateMachine.State != ChatMachineState.Disconnected)
			ChatStateMachine.MakeTransition(ChatMachineCommand.ToDisconnected);
	}

	private void HandleChatClientConnected(ClientLoginModel clientLogin)
	{
		ChatStateMachine.MakeTransition(ChatMachineCommand.ToConnected);

		chatView.SetName(clientLogin.Name);
	}

	private void HandleChatClientConnectionLost()
	{
		ChatStateMachine.MakeTransition(ChatMachineCommand.ToDisconnected);
	}
}
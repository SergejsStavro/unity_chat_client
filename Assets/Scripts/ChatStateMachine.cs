using com.bitvillain.fsm;

public enum ChatMachineState
{
	None = 0,
	Unconnected = 1,
	Connected = 2,
	Disconnected = 3,
}

public enum ChatMachineCommand
{
	None = 0,
	ToUnconnected = 1,
	ToConnected = 2,
	ToDisconnected = 3,
}

public class ChatMachineTransition : FiniteStateMachineTransition<ChatMachineState, ChatMachineCommand> { }
public class ChatStateMachine : FiniteStateMachine<ChatMachineState, ChatMachineTransition, ChatMachineCommand>
{
	public ChatStateMachine()
	{
		AddDirectedTransition(
			ChatMachineState.Unconnected,
			ChatMachineCommand.ToConnected,
			ChatMachineState.Connected
		);
		AddUndirectedTransition(
			ChatMachineState.Connected,
			ChatMachineCommand.ToDisconnected,
			ChatMachineState.Disconnected,
			ChatMachineCommand.ToConnected
		);
		SetInitialState(ChatMachineState.Unconnected);
	}
}
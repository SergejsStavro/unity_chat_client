﻿using System.Collections.Generic;

namespace com.bitvillain.fsm
{
	public class FiniteStateMachineTransition<S, C> : IFiniteStateMachineTransition<S, C>
	{
		S state;
		C command;

		public S State
		{
			get
			{
				return state;
			}
			set
			{
				state = value;
			}
		}

		public C Command
		{
			get
			{
				return command;
			}
			set
			{
				command = value;
			}
		}

		public FiniteStateMachineTransition() { }

		public FiniteStateMachineTransition(S state, C command)
		{
			this.state = state;
			this.command = command;
		}

		public override int GetHashCode()
		{
			int hash = 17;
			if (state == null || command == null)
			{
				hash = base.GetHashCode();
			}
			else
			{
				hash = hash * 31 + state.GetHashCode();
				hash = hash * 31 + command.GetHashCode();
			}

			return hash;
		}

		public override bool Equals(object otherTransitionObject)
		{
			var otherTransition =
				otherTransitionObject as FiniteStateMachineTransition<S, C>;

			return
				state != null &&
				command != null &&
				otherTransition != null &&
				EqualityComparer<S>.Default.Equals(state, otherTransition.state) &&
				EqualityComparer<C>.Default.Equals(command, otherTransition.command);
		}
	}
}
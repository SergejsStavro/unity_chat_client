﻿using System;
using System.Collections.Generic;

namespace com.bitvillain.chat.client
{
	public class Pool<T>
	{
		private readonly Func<T> factoryMethod;
		private int count;
		private int maxCount;
		private Queue<T> pool;

		public Pool(Func<T> factoryMethod, int maxCount = 10)
		{
			this.factoryMethod = factoryMethod;
			this.maxCount = maxCount;

			pool = new Queue<T>();
			count = 0;
		}

		public T Get()
		{
			T item = default(T);
			if (pool.Count == 0 && count < maxCount)
			{
				item = factoryMethod();
				count++;
			}
			else
			{
				item = pool.Dequeue();
			}
			return item;
		}

		public void Put(T item)
		{
			pool.Enqueue(item);
		}
	}
}
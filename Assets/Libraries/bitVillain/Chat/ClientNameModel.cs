namespace com.bitvillain.chat.client
{
	public class ClientNameModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
	}
}
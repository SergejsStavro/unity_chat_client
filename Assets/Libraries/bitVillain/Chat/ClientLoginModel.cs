namespace com.bitvillain.chat.client
{
	public class ClientLoginModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
	}
}
namespace com.bitvillain.chat.client
{
	public class RoomAdditionModel
	{
		public int RoomID { get; set; }
		public string Token { get; set; }
	}
}
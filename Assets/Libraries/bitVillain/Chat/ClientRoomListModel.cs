using System.Collections.Generic;

namespace com.bitvillain.chat.client
{
	public class ClientRoomListModel
	{
		public List<ClientRoomModel> Rooms { get; set; }
	}
}
﻿using System;

namespace com.bitvillain.chat.client
{
	public class ChatCommand
	{
		public ChatCommandType Type { get; set; }
		public string JSON { get; set; }
	}
}
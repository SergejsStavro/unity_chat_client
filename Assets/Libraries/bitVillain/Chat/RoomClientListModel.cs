using System.Collections.Generic;

namespace com.bitvillain.chat.client
{
	public class RoomClientListModel
	{
		public int RoomID { get; set; }
		public List<RoomClientModel> Clients { get; set; }
	}
}
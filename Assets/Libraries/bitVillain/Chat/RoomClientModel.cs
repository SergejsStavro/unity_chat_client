namespace com.bitvillain.chat.client
{
	public class RoomClientModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
	}
}
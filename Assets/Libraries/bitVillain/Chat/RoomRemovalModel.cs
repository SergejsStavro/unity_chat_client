namespace com.bitvillain.chat.client
{
	public class RoomRemovalModel
	{
		public int RoomID { get; set; }
		public string Token { get; set; }
	}
}
using System;

namespace com.bitvillain.chat.client
{
	public class ClientRoomMessageModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
		public int RoomID { get; set; }
		public string Message { get; set; }
		public DateTime Timestamp { get; set; }
	}
}
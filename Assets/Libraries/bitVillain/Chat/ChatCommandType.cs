﻿namespace com.bitvillain.chat.client
{
	public enum ChatCommandType
	{
		None = 0,
		ClientLogin = 1,
		ClientLogout = 2,
		ClientWrite = 3,
		ClientSetName = 4,
		ClientRoomList = 5, // room list

		RoomNew = 10,
		RoomClientList = 11, // client list
		RoomClientAdd = 12,
		RoomClientRemove = 13,
	}
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace com.bitvillain.chat.client
{
	public class ChatClient
	{
		const string emptyJSON = "{}";

		const int Timeout = 300;
		bool isConnected = false;
		bool isConnectionLost = false;
		int commandQueueCapacity = 10;

		int port = 8000;
		public int Port { get => port; set => port = value; }
		string hostname = "127.0.0.1";
		public string Hostname { get => hostname; set => hostname = value; }

		string clientToken = "no-token";
		TcpClient tcpClient;
		CancellationTokenSource readTokenSource;
		CancellationTokenSource writeTokenSource;

		Pool<ChatCommand> commandPool;
		Queue<ChatCommand> commandQueue = new Queue<ChatCommand>();

		Dictionary<ChatCommandType, Action<ChatCommand>> commandTypeToHandler;

		public bool Connected { get => isConnected; }
		public string ClientToken { get => clientToken; }

		public event Action<ClientLoginModel> ClientConnected = (token) => { };
		public event Action ClientDisconnected = () => { };
		public event Action ClientConnectionSucceeded = () => { };
		public event Action<bool> ClientConnectionFailed = (_) => { };
		public event Action ClientConnectionLost = () => { };
		public event Action ClientConnectionTimedOut = () => { };

		public event Action<ClientNameModel> ClientNameReceived = (name) => { };
		public event Action<ClientRoomMessageModel> ClientRoomMessageReceived = (message) => { };
		public event Action<ClientRoomListModel> ClientRoomListReceived = (roomIDs) => { };
		public event Action<RoomClientListModel> RoomClientListReceived = (clients) => { };

		public ChatClient()
		{
			commandPool = new Pool<ChatCommand>(
				() => { return new ChatCommand(); },
				commandQueueCapacity
			);

			commandTypeToHandler = new Dictionary<ChatCommandType, Action<ChatCommand>>()
			{

				{ ChatCommandType.ClientLogin, HandleClientLogin },

				{ ChatCommandType.ClientWrite, HandleClientWrite },

				{ ChatCommandType.ClientSetName, HandleClientSetName },

				{ ChatCommandType.ClientRoomList, HandleClientRoomList },

				{ ChatCommandType.RoomClientList, HandleRoomClientList },

			};
		}

		public void Connect()
		{
			ConnectAsync();
		}

		private async Task ConnectAsync()
		{
			try
			{
				if (!isConnected)
				{
					if(readTokenSource != null)
						readTokenSource.Cancel();
					if(writeTokenSource != null)
						writeTokenSource.Cancel();

					tcpClient = new TcpClient();
					readTokenSource = new CancellationTokenSource();
					writeTokenSource = new CancellationTokenSource();

					int timeout = Timeout;
					do
					{
						try
						{
							await tcpClient.ConnectAsync(hostname, port);
						}
						catch (Exception e)
						{
							ClientConnectionFailed(isConnectionLost);

							await Task.Delay(1000);
						}
					} while (!tcpClient.Connected && timeout-- > 0);

					if (tcpClient.Connected)
					{
						isConnected = true;

						ClientConnectionSucceeded();

						await LoginClient();

						ReadClientStreamAsync(tcpClient);

						UpdateAsync();
					}
					else
					{
						ClientConnectionTimedOut();
						isConnected = false;
					}
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		private async Task UpdateAsync()
		{
			try
			{
				while (isConnected)
				{
					await Task.Delay(30);

					ExecuteQueuedCommands();
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		private async Task ExecuteQueuedCommands()
		{
			ChatCommand command;
			Action<ChatCommand> commandHandler;
			for (int i = 0; commandQueue.Count > 0 && i < commandQueueCapacity; i++)
			{
				command = commandQueue.Dequeue();
				if (command != null &&
					commandTypeToHandler.TryGetValue(command.Type, out commandHandler))
				{
					commandHandler(command);
				}

				commandPool.Put(command);
			}
		}

		#region incoming

		private void HandleClientLogin(ChatCommand command)
		{
			var clientLogin = JsonConvert.DeserializeObject<ClientLoginModel>(command.JSON);
			clientToken = clientLogin.Token;
			ClientConnected(clientLogin);
		}

		private void HandleClientRoomList(ChatCommand command)
		{
			ClientRoomListReceived(
				JsonConvert.DeserializeObject<ClientRoomListModel>(command.JSON)
			);
		}

		private void HandleClientSetName(ChatCommand command)
		{
			ClientNameReceived(
				JsonConvert.DeserializeObject<ClientNameModel>(command.JSON)
			);
		}

		private void HandleRoomClientList(ChatCommand command)
		{
			RoomClientListReceived(
				JsonConvert.DeserializeObject<RoomClientListModel>(command.JSON)
			);
		}

		private void HandleClientWrite(ChatCommand command)
		{
			ClientRoomMessageReceived(
				JsonConvert.DeserializeObject<ClientRoomMessageModel>(command.JSON)
			);
		}

		private async Task ReadClientStreamAsync(TcpClient client)
		{
			try
			{
				if (client != null)
				{
					NetworkStream stream = client.GetStream();

					byte[] commandBuffer = new byte[4];
					byte[] jsonBytesBuffer;
					byte[] jsonBytesCountBuffer = new byte[4];

					ChatCommandType commandType = ChatCommandType.ClientRoomList;
					int jsonBytesCount = 0;
					int jsonBytesRead = 0;

					while (client.Connected && commandType != ChatCommandType.None)
					{
						var command = commandPool.Get();
						if (command != null)
						{
							commandType = await ReadClientStreamCommandAsync(client, commandBuffer, readTokenSource);

							command.Type = commandType;

							if (commandType != ChatCommandType.None)
							{
								jsonBytesCount = await ReadClientStreamMessageLengthAsync(client, jsonBytesCountBuffer, readTokenSource);

								jsonBytesBuffer = new byte[jsonBytesCount];
								jsonBytesRead = await stream.ReadAsync(jsonBytesBuffer, 0, jsonBytesCount, readTokenSource.Token);

								command.JSON = Encoding.UTF8.GetString(jsonBytesBuffer, 0, jsonBytesCount);

								commandQueue.Enqueue(command);
							}
						}
					}

					if (isConnected)
					{
						isConnectionLost = true;
						ClientConnectionLost();
						Disconnect();
						Connect();
					}
				}
			}
			catch (Exception e)
			{
				Disconnect();
				Debug.WriteLine(e.ToString());
			}
		}

		private async Task<ChatCommandType> ReadClientStreamCommandAsync(TcpClient tcpClient, byte[] commandBuffer, CancellationTokenSource tokenSource)
		{
			ChatCommandType commandType = ChatCommandType.None;

			if (tcpClient != null && tcpClient.Connected)
			{
				int charactersRead = 0;

				try
				{
					NetworkStream stream = tcpClient.GetStream();
					charactersRead = await stream.ReadAsync(commandBuffer, 0, commandBuffer.Length, tokenSource.Token);
				}
				catch (Exception e)
				{
					Disconnect();
					Debug.WriteLine(e.ToString());
				}

				if (charactersRead > 0)
				{
					int commandInt = BitConverter.ToInt32(commandBuffer, 0);
					if (Enum.IsDefined(typeof(ChatCommandType), commandInt))
						commandType = (ChatCommandType)commandInt;

					Array.Clear(commandBuffer, 0, commandBuffer.Length);
				}
			}

			return commandType;
		}

		private async Task<int> ReadClientStreamMessageLengthAsync(TcpClient client, byte[] sizeBuffer, CancellationTokenSource tokenSource)
		{
			int messageLength = 0;

			if (tcpClient != null && tcpClient.Connected)
			{
				int charactersRead = 0;

				try
				{
					NetworkStream stream = tcpClient.GetStream();
					charactersRead = await stream.ReadAsync(sizeBuffer, 0, sizeBuffer.Length, tokenSource.Token);
				}
				catch (Exception e)
				{
					Disconnect();
					Debug.WriteLine(e.ToString());
				}

				if (charactersRead > 0)
					messageLength = BitConverter.ToInt32(sizeBuffer, 0);

				Array.Clear(sizeBuffer, 0, sizeBuffer.Length);
			}

			return messageLength;
		}

		#endregion

		#region outgoing
		public async Task LoginClient()
		{
			var data = new ClientLoginModel() { Token = clientToken };
			var json = JsonConvert.SerializeObject(data);

			await WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.ClientLogin, json));
		}

		public async Task LogoutClient()
		{
			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.ClientLogout, emptyJSON));
		}

		public void SendRoomMessage(int roomID, string message)
		{
			var data = new ClientRoomMessageModel()
			{
				Name = string.Empty,
					RoomID = roomID,
					Message = message,
					Timestamp = DateTime.UtcNow
			};
			var json = JsonConvert.SerializeObject(data);

			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.ClientWrite, json));
		}

		public void SetName(string name)
		{
			var data = new ClientNameModel() { Name = name };
			var json = JsonConvert.SerializeObject(data);

			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.ClientSetName, json));
		}

		public void FetchClientList(int roomID)
		{
			var data = new ClientRoomModel() { RoomID = roomID };
			var json = JsonConvert.SerializeObject(data);

			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.RoomClientList, json));
		}

		public void CreateNewRoom()
		{
			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.RoomNew, emptyJSON));
		}

		public void AddToRoom(int roomID, string token)
		{
			var data = new RoomAdditionModel() { RoomID = roomID, Token = token };
			var json = JsonConvert.SerializeObject(data);

			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.RoomClientAdd, json));
		}

		public void RemoveFromRoom(int roomID, string token)
		{
			var data = new RoomRemovalModel() { RoomID = roomID, Token = token };
			var json = JsonConvert.SerializeObject(data);

			WriteClientStreamAsync(tcpClient, Serialize(ChatCommandType.RoomClientRemove, json));
		}

		private byte[] Serialize(ChatCommandType commandType, string message)
		{
			byte[] commandBytes = Serialize(commandType);
			byte[] messageBytes = Serialize(message);

			return commandBytes.Concat(messageBytes).ToArray();
		}

		private byte[] Serialize(ChatCommandType commandType)
		{
			// command: commandData[4 bytes]
			return BitConverter.GetBytes((int)commandType);
		}

		private byte[] Serialize(string source)
		{
			// string: stringSize[4 bytes] + stringData[stringSize]
			byte[] bytes = Encoding.UTF8.GetBytes(source);
			byte[] bytesSize = BitConverter.GetBytes(bytes.Length);
			return bytesSize.Concat(bytes).ToArray();
		}

		private async Task WriteClientStreamAsync(TcpClient client, byte[] requestBytes)
		{
			if (client != null && client.Connected)
			{
				try
				{
					NetworkStream stream = client.GetStream();
					if (stream.CanWrite)
						await stream.WriteAsync(requestBytes, 0, requestBytes.Length, writeTokenSource.Token);
				}
				catch (Exception e)
				{
					Disconnect();
					Debug.WriteLine(e.ToString());
				}
			}
		}

		#endregion

		public void Disconnect()
		{
			DisconnectAsync();
		}

		private async Task DisconnectAsync()
		{
			if (isConnected)
			{
				await LogoutClient();

				isConnected = false;

				readTokenSource.Cancel();
				writeTokenSource.Cancel();

				if (tcpClient != null)
					tcpClient.Close();

				ClientDisconnected();
			}
		}
	}
}